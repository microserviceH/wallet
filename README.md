# Aplikasi Wallet Service #
## Setup Database ##

* Login ke database sebagai root

        $ mysql -u root 
    
      Welcome to the MySQL monitor.  Commands end with ; or \g.
      Your MySQL connection id is 2
      Server version: 5.7.23 Homebrew
    
      Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.
    
      Oracle is a registered trademark of Oracle Corporation and/or its
      affiliates. Other names may be trademarks of their respective
      owners.
    
      Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    
        mysql>

    
* Buat database 
    
        Create database walletdb
    
* Buat user untuk mengakses database(MySQL 8)

        mysql> CREATE USER 'wallet'@'localhost' IDENTIFIED WITH mysql_native_password BY 'wallet123';
        Query OK, 0 rows affected (0.25 sec)
    
        mysql> grant all privileges on walletdb.* to wallet@localhost;
        Query OK, 0 rows affected (0.18 sec)
        
* Jalankan Aplikasi

        mvn spring-boot:run