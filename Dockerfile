FROM openjdk:8-alpine
COPY target/*.jar /opt/
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/*.jar"]