package chubb.training.microservices.wallet.entity;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity @Data
public class Wallet {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    @NotEmpty
    private String code;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_owner")
    private WalletOwner owner;

    @NotNull @Min(0)
    private BigDecimal balance;
}