package chubb.training.microservices.wallet.entity;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity @Data
public class WalletTransaction {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_wallet")
    private Wallet wallet;


    private LocalDateTime transactionTime = LocalDateTime.now();

    @NotEmpty
    private String description;

    @NotNull @Enumerated(EnumType.STRING)
    private TransactionType transactionType;
}