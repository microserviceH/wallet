package chubb.training.microservices.wallet.dao;

import chubb.training.microservices.wallet.entity.Wallet;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WalletDao extends PagingAndSortingRepository<Wallet,String> {

}
