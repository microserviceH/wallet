package chubb.training.microservices.wallet.entity;

public enum  TransactionType {
    TOPUP,PAYMENT,PURCHASE
}
