package chubb.training.microservices.wallet.entity;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Entity @Data
public class WalletOwner {

    @Id @GeneratedValue(generator="uuid")
    @GenericGenerator(name="uuid", strategy = "uuid2")
    private String id;
    @NotEmpty
    private String name;
    @NotEmpty @Email
    private String email;
    private String mobilePhone;

}
