package chubb.training.microservices.wallet.controller;

import chubb.training.microservices.wallet.dao.WalletDao;
import chubb.training.microservices.wallet.entity.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WalletController {
    @Autowired private WalletDao walletDao;

    @GetMapping
    public Page<Wallet> semuaWallet(Pageable page){
        return walletDao.findAll(page);
    }

}
